package time_it

import "core:time"
import "core:mem"

Time :: time.Time
Duration :: time.Duration

Sample :: struct {
	name: string,
	duration: Duration,
}

Benchmark :: struct {
	samples: [dynamic]Sample,
}

benchmark_init :: proc (b: ^Benchmark, allocator := context.allocator) {
	b.samples = make([dynamic]Sample, allocator = allocator)
}

benchmark_destroy :: proc(b: ^Benchmark){
	delete(b.samples)
}

@(deferred_in_out=sample_finish)
sample :: proc(b: ^Benchmark, name: string) -> time.Tick {
	return time.tick_now()
}

@private
sample_finish :: proc(b: ^Benchmark, name: string, last: time.Tick){
	delta := time.tick_since(last)
	append(&b.samples, Sample{
		name = name,
		duration = delta,
	})
}

