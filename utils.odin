package editor_tui

import "core:fmt"
import "core:time"
import "core:mem"
import "core:slice"
import str   "core:strings"
import ucode "core:unicode"
import utf   "core:unicode/utf8"
import os    "core:os/os2"

import       "time_it"
import term  "ansi_term"
import tb    "text_editor/text_buffer"
import ed    "text_editor"

report_stats :: proc(bench: time_it.Benchmark){
	fmt.println("\n")
	fmt.println("--- PERFORMANCE REPORT ---")
	defer fmt.println("--------------------------")

	info_map := make(map[string][dynamic]time_it.Duration, allocator = context.temp_allocator)

	for sample in bench.samples {
		list, ok := info_map[sample.name]
		if !ok {
			info_map[sample.name] = make([dynamic]time_it.Duration, allocator = context.temp_allocator)
		}
		append(&info_map[sample.name], sample.duration)
	}

	format :: "%v [%v]\n"+
	          "  min: %v\n"+
	          "  max: %v\n"+
	          "  mean: %v\n"+
	          "  median: %v\n"

	mean :: proc(s: $T/[]$E) -> E {
		total : E
		for v in s {
			total += v
		}
		return total / E(len(s))
	}

	median :: proc(s: []time.Duration) -> time.Duration {
		if len(s) == 0 { return 0 }
		slice.sort(s)
		return s[len(s) / 2]
	}

	for name, durations in info_map {
		mini, maxi, _ := slice.min_max(durations[:])
		fmt.printf(format, name, len(durations), mini, maxi, mean(durations[:]), median(durations[:]))
	}
}

error_message :: proc(msg: string, width, height: int) {
	term.clear_screen()
	term.set_color(.White, .Red, true)
	y := max((width / 2) - (len(msg) / 2), 0)
	term.move_cursor({height / 2, y})
	fmt.println(msg)
	term.reset_styling()
	time.sleep(750 * time.Millisecond)
}

slice_shift_right :: proc(s: []$T, n: int) -> []T {
	preserve := slice.clone(s[:len(s) - n], allocator = context.temp_allocator)
	for e, i in preserve {
		s[n + i] = e
	}
	mem.set(raw_data(s), 0, n*size_of(T))
	return s
}

slice_shift_left :: proc(s: []$T, n: int) -> []T{
	preserve := slice.clone(s[n:], allocator = context.temp_allocator)
	for e, i in preserve {
		s[i] = e
	}
	overwrite := s[len(s) - n:]
	mem.set(raw_data(overwrite), 0, len(overwrite) * size_of(T))
	return s
}

STDIN_FD  :: 0
STDOUT_FD :: 1
STDERR_FD :: 2

