#!/usr/bin/env sh

set -e
Run() { echo "$@" > /dev/stderr ; $@; }

[ -z "$CC" ] && CC=cc
CFLAGS="$CFLAGS -O2 -fPIC -fno-strict-aliasing"

Run $CC $CFLAGS -c ansi_term/tty_linux.c -o ansi_term/tty_linux.o
Run odin build . -o:speed -reloc-mode:pic -thread-count:$(($(nproc) * 2))

