package ansi_term

import "core:io"
import "core:os"
import "core:fmt"

CSI :: "\e["

Color :: enum {
	Black     = 0x0,
	Red       = 0x1,
	Green     = 0x2,
	Yellow    = 0x3,
	Blue      = 0x4,
	Magenta   = 0x5,
	Cyan      = 0x6,
	White     = 0x7,
}

Cursor_Dir :: enum byte {
	Up = 'A', Down = 'B', Right = 'C', Left = 'D',
}

move_cursor :: proc {move_cursor_dir, move_cursor_pos}

move_cursor_dir :: proc(amount: int, dir: Cursor_Dir){
	r := byte(dir)
	fmt.printf(CSI + "%d%c", amount, r)
}

move_cursor_pos :: proc(pos: [2]int){
	fmt.printf(CSI + "%d;%dH", pos.x, pos.y)
}

scroll_up :: proc(amount: int) {
	fmt.printf(CSI + "%dS", amount)
}

scroll_down :: proc(amount: int) {
	fmt.printf(CSI + "%dT", amount)
}

clear_screen :: proc() {
	fmt.printf(CSI + "2J" + CSI + "3J")
}

set_color :: proc(fg, bg: Maybe(Color), bold := false){
	fg_set := fg != nil
	fmt.printf(CSI + "%d;", int(bold))

	if fg_set {
		fmt.printf("3%d", int(fg.?))
	}

	if bg != nil {
		fmt.printf("%s4%d", ";" if fg_set else "", int(bg.?))
	}
	fmt.print("m")
}

reset_styling :: proc(){
	fmt.printf(CSI + "0m")
}


