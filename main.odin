package editor_tui

import "core:fmt"
import "core:time"
import "core:mem"
import "core:slice"
import str   "core:strings"
import ucode "core:unicode"
import utf   "core:unicode/utf8"
import os    "core:os/os2"

import term  "ansi_term"
import tb    "text_editor/text_buffer"
import ed    "text_editor"

import "time_it"

stats : time_it.Benchmark

visible_buffer := 0
app_running := true

// TODO: Better way to handle line updates

// Uses context.temp_allocator
draw_statusbar :: proc(editor: ed.Editor, width: int, height: int){
	time_it.sample(&stats, #procedure)
	term.move_cursor({1,1})
	term.set_color(.Yellow, .Black, false)
	defer term.reset_styling()

	buf := make([]byte, width + 1, context.temp_allocator)

	buffer := editor.buffers[visible_buffer].buffer
	row, col := tb.cursor_to_text_coordinate(buffer)

	under_cur := utf.RUNE_ERROR
	if pos := buffer.cursor.pos; pos < tb.text_len(buffer) {
		under_cur, _ = tb.rune_at(buffer, pos)
	}
	rune_under_cur, n := utf.encode_rune(under_cur)

	cur_display := string(rune_under_cur[:n]) if under_cur != '\n' else `LF`
	if tb.cursor_at_end(buffer) {
		cur_display = `EOF`
	}

	info := fmt.bprintf(buf, "{:s} | {:v} -> R:{:v} C:{:v} | CUR: [{:v}] ",
		editor.buffers[visible_buffer].fullpath,
		buffer.cursor.pos, row, col,
		cur_display)

	pad, _ := str.repeat(" ", max(0, width - len(info)), context.temp_allocator)

	fmt.println(info, pad, sep="")
}

main :: proc(){
	when ODIN_DEBUG {
		tracker_ally : mem.Tracking_Allocator
		defer mem.tracking_allocator_destroy(&tracker_ally)
		mem.tracking_allocator_init(&tracker_ally, context.allocator)
		context.allocator = mem.tracking_allocator(&tracker_ally)
		defer report_tracking_allocator(tracker_ally)

		time_it.benchmark_init(&stats)
		defer time_it.benchmark_destroy(&stats)
		defer report_stats(stats)
	}

	defer free_all(context.temp_allocator)

	cols, rows := term.get_dimensions()

	visible_text, _ := view_make(rows - 1)

	term.clear_screen()
	term.enable_raw_mode(STDIN_FD)
	defer term.disable_raw_mode(STDIN_FD)

	editor, _ := ed.editor_make()
	defer ed.editor_destroy(&editor)

	_ = ed.add_empty_buffer(&editor)

	for app_running {
		defer free_all(context.temp_allocator)
		input_buf := [256]u8{}

		render_text: {
			term.move_cursor({1, 1})
			term.reset_styling()
			term.clear_screen()

			draw_statusbar(editor, cols, rows)

			render_buffer: {
				// TODO: Only clone and replace visible lines when they change
				buf := editor.buffers[visible_buffer].buffer._edit_impl

				part1, _ := str.replace_all(string(buf.data[:buf.gap_start]), "\t", "    ", context.temp_allocator)
				part2, _ := str.replace_all(string(buf.data[buf.gap_end:]), "\t", "    ", context.temp_allocator)

				fmt.printf("%s%s", part1, part2)
			}
		}

		postion_cursor: {
			row, col := tb.cursor_to_text_coordinate(editor.buffers[visible_buffer].buffer)
			// +1 because of statusbar
			term.move_cursor({row+1, col})
		}

		input := term.read(input_buf[:])

		r, n := utf.decode_rune(input)

		buffer := &editor.buffers[visible_buffer].buffer
		switch {
		case ucode.is_control(r):
			switch r {
			case '\n', '\r':
				_ = tb.insert(buffer, buffer.cursor.pos, input)
				buffer.cursor.pos += len(input)
				tb.update_lines(buffer)

			case '\t':
				_ = tb.insert(buffer, buffer.cursor.pos, input)
				buffer.cursor.pos += len(input)

			case 4:
				app_running = false

			case 127:
				if tb.text_len(buffer^) > 0 {
					prev := buffer.cursor.pos
					tb.move_cursor_runes(buffer, -1)
					range := [2]int{buffer.cursor.pos, prev}
					_ = tb.remove(buffer, range)
					// TODO: only update if a '\n' was deleted
					tb.update_lines(buffer)
				}

			case:
				UP :: []byte{ 27, '[', 'A' }
				DOWN :: []byte{ 27, '[', 'B' }
				RIGHT :: []byte{ 27, '[', 'C' }
				LEFT :: []byte{ 27, '[', 'D' }

				if len(input) == 3 {
					if slice.equal(LEFT, input[:3]) {
						tb.move_cursor_runes(buffer, -1)
						continue
					}
					else if slice.equal(RIGHT, input[:3]) {
						tb.move_cursor_runes(buffer, 1)
						continue
					}
					else if slice.equal(UP, input[:3]){
						tb.move_cursor_up(buffer)
						continue
					}
					else if slice.equal(DOWN, input[:3]){
						tb.move_cursor_down(buffer)
						continue
					}
				}

				unknown_sequence : {
					error_message(fmt.tprintf("Unknown input sequence: %v", input), cols, rows)
				}
			}
		case:
			insert_rune: {
				time_it.sample(&stats, "Insert rune")
				tb.insert(buffer, buffer.cursor.pos, input)
				buffer.cursor.pos += len(input)

				if !tb.cursor_at_end(buffer^){
					cursor_on_newline := tb.byte_at(buffer^, buffer.cursor.pos) == '\n'
					if cursor_on_newline {
						tb.update_lines(buffer)
					}
				}
			}
		}

	}
}

report_tracking_allocator :: proc(ally: mem.Tracking_Allocator) {
	fmt.println("\n")
	fmt.println("--- TRACKING ALLOCATOR REPORT ---")
	fmt.println("Address    | File")
	defer fmt.println("---------------------------------")
	for ptr, info in ally.allocation_map {
		fmt.printf("%08v | %v\n", ptr, info.location)
	}
}

