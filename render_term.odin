package editor_tui

import "core:mem"

import os "core:os/os2"
import tb "text_editor/text_buffer"

Buffer_View :: struct {
	begin: int,
	lines: [][]byte,

	allocator: mem.Allocator,
}

view_make :: proc(size: int, allocator := context.allocator) -> (bv: Buffer_View, err: mem.Allocator_Error) {
	bv.lines = make([][]byte, size) or_return
	bv.begin = 0
	return
}

view_load :: proc(bv: ^Buffer_View, buf: tb.Text_Buffer) -> (ok := true) {
	return
}

view_scoll_down :: proc(bv: ^Buffer_View, buf: tb.Text_Buffer, amount := 1) -> (ok := true){
	bv.lines = slice_shift_left(bv.lines, amount)
	bv.begin = min(len(buf.lines), bv.begin + amount)

	to_fill := bv.lines[:len(bv.lines) - amount]
	for _, i in to_fill {
		linedata, err:= tb.clone_line(buf, i, allocator = bv.allocator)
		if err != nil { return false }
		to_fill[i] = linedata
	}

	return
}

render_buffer_view :: proc(bv: Buffer_View, console: ^os.File){
	for l in bv.lines {
		os.write(console, l)
	}
}


